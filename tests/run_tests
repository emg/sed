#!/bin/sh

warn() {
    fmt=$1
    shift
    printf "\n$fmt\n" "$@" >&2
    wflag=1
}

die() {
    es=$1
    shift
    warn "$@"
    exit "$es"
}

check_files() {
    # same .input and .good for .sh and .sed, different .output and .error
    script=$1
    input=${script%.*}.input
    good=${script%.*}.good
    output=$script.output
    error=$script.error

    # the script really should exist seeing as we used the glob... but maybe
    # that'll change in the future
    [ -r "$script" ] || warn "script file (%s) does not exist or is not readable" "$script"
    [ -r "$input"  ] || warn  "input file (%s) does not exist or is not readable" "$input"
    [ -r "$good"   ] || warn   "good file (%s) does not exist or is not readable" "$good"
}

pass_fail() {
    if [ "$wflag" ]; then printf fail\\n; gfail=$((gfail + 1))
    else                  printf pass\\n
    fi
}

export LC_ALL=POSIX
# so we can override with an environment variable
# also use $FLAGS so we can do
# e.g. SED=/usr/bin/sed FLAGS=--posix ./run_tests
# quoting in FLAGS will cause pain (it was only added for the above example)
export SED=${SED:-../sed}
command -v "$SED" > /dev/null || die 1 "sed (%s) does not exist or is not executable" "$SED"

unset gfail
max=0
for script in *.sed *.sh; do
    [ "${#script}" -gt "$max" ] && max=${#script}
done

for file in *.sed; do
    unset wflag
    printf "%-*s: " "$max" "$file"
    check_files "$file"

    # run the test, if sed fails set wflag
    "$SED" $FLAGS -f "./$script" "./$input" > "$output" 2> "$error" || wflag=1

    # if sed succeeded compare output. if output differs set wflag
    [ "$wflag" ] || cmp -s "./$output" "./$good" || wflag=1

    # print pass or fail
    pass_fail
done

for file in *.sh; do
    unset wflag
    printf "%-*s: " "$max" "$file"
    check_files "$file"

    sh "./$script" "./$input" > "$output" 2> "$error" || wflag=1
    [ "$wflag" ] || cmp -s "./$output" "./$good"      || wflag=1
    pass_fail
done

printf "\n%d failures\n" "$gfail"
! [ "$gfail" ]
