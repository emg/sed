#define _POSIX_C_SOURCE 200809L
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <regex.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_WFILES  ((size_t)  10) // minimum required by POSIX
#define LABEL_BYTES ((size_t)   9) // minimum required by POSIX + '\0'
#define SPACE_BYTES ((size_t)8193) // minimum required by POSIX + '\0'

#define LENGTH(a) (sizeof(a) / sizeof(*a))
#define USE(a)    ((void)(a)) // to escape unused warnings
#define serror()  ((void)fprintf(stderr, "%s: %zu: %s,%s,%d: %s\n", prog_name, line_number, __FILE__, __func__, __LINE__, strerror(errno)))
#define warn(...) do{ fprintf(stderr, "%s: %zu: ", prog_name, line_number); fprintf(stderr, __VA_ARGS__); fprintf(stderr, "\n"); }while(0)

// Types
typedef struct {
    union {
        size_t  line_number;
        regex_t *regex;
    };
    enum {
        IGNORE, // empty address, ignore
        EVERY , // every line
        LINE  , // line number
        LAST  , // last line ($)
        REGEX , // use included regex
        LASTRE, // use most recently used regex
    } type;
} Address;

// naddr == 0 iff beg.type == EVERY  && end.type == IGNORE
// naddr == 1 iff beg.type != IGNORE && end.type == IGNORE
// naddr == 2 iff beg.type != IGNORE && end.type != IGNORE
typedef struct {
    Address       beg;
    Address       end;
    unsigned char naddr; // not necessary, but helpful
} Range;

typedef struct _command Command; // forward declaration for function pointer types

typedef int    (*Action )(void           ); // Action taken after current command executes
typedef Action (*Sedfunc)(Command*       ); // Sed function, takes current command
typedef int    (*Print  )(char*   , FILE*); // Print function for a,c,i,r
typedef char  *(*Getarg )(Command*, char*); // Read arguments for given command from line
typedef void   (*Freearg)(Command*       ); // Free anything that was malloced in Getarg

typedef struct {    // used to build the lookup table of functions
    Sedfunc  runcmd;
    Getarg   getarg;
    Freearg  freearg;
    unsigned naddr; // max addresses function takes
} Sedfunc_info;

// TODO: worth switching back to mallocing explicit types and just having a
//       void *extra instead of builtin types in the union?
struct _command {
    Range         range;
    Sedfunc_info *func;
    union {
        Command  *jump;                  // used for   b,t during execution
        char      label[LABEL_BYTES];    // used for :,b,t during building
        ptrdiff_t offset;                // used for { (instead of jump because pointers break when we realloc)
        struct {                         // used for a,c,i,r,s,y
            union {
                regex_t   *regex;        // for s regex to find
                Print      print;        // for a,c,i check_puts for r write_file
            };
            char          *text;         // y sets, s replace text, a,c,i,r text
            size_t         size;         // size of malloced char *text string
            FILE          *file;         // for w, and for s: if non NULL, file for w flag
            unsigned short occurrence;   // which match to replace, 0 for all (g flag)
            char           delim;        // s/// delimiter
            unsigned char  flag_p    :1; // print flag
            unsigned char  last_regex:1; // use last regex, not included regex
        };
    };
    unsigned char in_match:1;            // are we currently inside of a match
    unsigned char negate  :1;            // is the range negated for this command
};

typedef struct {    // used with push/pop as a dynamicly sized array
    Command **data; // was void *, any reason to keep void *?
    size_t    size;
    size_t    capacity;
} Vector;

// Globals
struct { // files for w commands and s///w flag
    char *name;
    FILE *file;
} wfiles[MAX_WFILES];

struct { // global flags
    unsigned char n        :1; // -n (no print)
    unsigned char s        :1; // s/// happened
    unsigned char aci_cont :1; // a,c,i text continuation
    unsigned char s_cont   :1; // s replacement text continuation
} gflags;

// store addresses of { in braces, : in labels, bt in branches during building
// store addresses of scheduled writes during execution
// store (cmd - prog) for braces, labels, and branches so we don't screw up
// pointers when we realloc
Vector braces, labels, branches, writes;

// pattern and hold spaces
char  space1[SPACE_BYTES],  space2[SPACE_BYTES];
char *patt_space = space1, *hold_space = space2;

regex_t *last_regex;     // last used regex for use with empty regex searches
char   **files;          // list of file names from argv
FILE    *file;           // current file we are reading
char    *prog_name;      // argv[0]
Command *prog, *pc, *ni; // program memory, program counter, next instruction
size_t   prog_size;      // number of Commands in prog
size_t   line_number;    // of script when building, of input when running

char bre_escaped[] = "(){}123456789n" // 9.3.2 BRE Ordinary Characters plus \n for sed
                     ".[\\*^$";       // 9.3.3 BRE Special  Characters

// Forward Delcarations
// Sedfuncs
Action cmd_a     (Command *cmd);
Action cmd_b     (Command *cmd);
Action cmd_c     (Command *cmd);
Action cmd_d     (Command *cmd);
Action cmd_D     (Command *cmd);
Action cmd_g     (Command *cmd);
Action cmd_G     (Command *cmd);
Action cmd_h     (Command *cmd);
Action cmd_H     (Command *cmd);
Action cmd_i     (Command *cmd);
Action cmd_l     (Command *cmd);
Action cmd_n     (Command *cmd);
Action cmd_N     (Command *cmd);
Action cmd_p     (Command *cmd);
Action cmd_P     (Command *cmd);
Action cmd_q     (Command *cmd);
Action cmd_r     (Command *cmd);
Action cmd_s     (Command *cmd);
Action cmd_t     (Command *cmd);
Action cmd_w     (Command *cmd);
Action cmd_x     (Command *cmd);
Action cmd_y     (Command *cmd);
Action cmd_colon (Command *cmd);
Action cmd_equal (Command *cmd);
Action cmd_lbrace(Command *cmd);
Action cmd_rbrace(Command *cmd);
Action cmd_last  (Command *cmd);

// Getargs / Freeargs
void  free_acir_arg (Command *cmd);
void  free_s_arg    (Command *cmd);
void  free_y_arg    (Command *cmd);
char *get_aci_arg   (Command *cmd, char *str);
char *get_bt_arg    (Command *cmd, char *str);
char *get_colon_arg (Command *cmd, char *str);
char *get_lbrace_arg(Command *cmd, char *str);
char *get_r_arg     (Command *cmd, char *str);
char *get_rbrace_arg(Command *cmd, char *str);
char *get_s_arg     (Command *cmd, char *str);
char *get_w_arg     (Command *cmd, char *str);
char *get_y_arg     (Command *cmd, char *str);

// Actions
int app_line(void); // append new line, continue current cycle
int error   (void); // he's dead Jim
int new_line(void); // read   new line, continue current cycle
int new_next(void); // move to new cycle, read new line
int noop    (void); // continue execution
int old_next(void); // move to new cycle, reuse pattern space
int quit    (void); // do not start a new cycle

// Utilities
int      check_puts    (char *str, FILE *stream);
char    *chomp         (char *str);
int      resize        (void **ptr, size_t *nmemb, size_t size, size_t new_nmemb, void **next, int clear);
char    *find_delim    (char *str, char *allow, char delim, int do_brackets);
size_t   handle_escapes(char *beg, char *end, char delim, int n_newline);
int      is_eof        (FILE *stream);
int      next_file     (void);
Command *pop           (Vector *vec);
size_t   push          (Vector *vec, Command *cmd);
int      read_line     (char *buf, size_t size, FILE* file);
int      sized_memcpy  (char **dest, char *src, size_t n, char *buf, size_t buf_size);
void     strlcpy       (char *dest, char *src, size_t size);
void     strlcat       (char *dest, char *src, size_t size);
int      write_file    (char *in_path, FILE *out_stream);

// Build and run
int      aci_append   (Command *cmd, char *str);
int      build        (char *line);
void     cleanup      (void);
int      do_writes    (void);
Command *find_label   (Vector *vec, char *label);
int      in_range     (Command *cmd);
int      insert_labels(void);
char    *make_address (Address *addr, char *str);
char    *make_range   (Range *range, char *str);
int      match_addr   (Address *addr);
int      run          (void);
int      script_file  (char *path);
int      script_string(char *script);
void     update_ranges(Command *beg, Command *end);

// Lookup table of functions
Sedfunc_info funcs[] = {
    ['a'] = { cmd_a     , get_aci_arg   , free_acir_arg , 1 }, // schedule write of text for later
    ['b'] = { cmd_b     , get_bt_arg    , NULL          , 2 }, // branch to label (extra holds char *label while building, Command *label while running)
    ['c'] = { cmd_c     , get_aci_arg   , free_acir_arg , 2 }, // delete pattern space, at 0 or 1 addr or end of 2 addr, write text
    ['d'] = { cmd_d     , NULL          , NULL          , 2 }, // delete pattern space
    ['D'] = { cmd_D     , NULL          , NULL          , 2 }, // delete to first newline and start new cycle without reading (if no newline, d)
    ['g'] = { cmd_g     , NULL          , NULL          , 2 }, // replace pattern space with hold space
    ['G'] = { cmd_G     , NULL          , NULL          , 2 }, // append newline and hold space to pattern space
    ['h'] = { cmd_h     , NULL          , NULL          , 2 }, // replace hold space with pattern space
    ['H'] = { cmd_H     , NULL          , NULL          , 2 }, // append newline and pattern space to hold space
    ['i'] = { cmd_i     , get_aci_arg   , free_acir_arg , 1 }, // write text
    ['l'] = { cmd_l     , NULL          , NULL          , 2 }, // write pattern space in 'visually unambiguous form'
    ['n'] = { cmd_n     , NULL          , NULL          , 2 }, // write pattern space (unless -n) read to replace pattern space (if no input, quit)
    ['N'] = { cmd_N     , NULL          , NULL          , 2 }, // append to pattern space separated by newline, line number changes (if no input, quit)
    ['p'] = { cmd_p     , NULL          , NULL          , 2 }, // write pattern space
    ['P'] = { cmd_P     , NULL          , NULL          , 2 }, // write pattern space up to first newline
    ['q'] = { cmd_q     , NULL          , NULL          , 1 }, // quit
    ['r'] = { cmd_r     , get_r_arg     , free_acir_arg , 1 }, // write contents of file (unable to open/read treated as empty file)
    ['s'] = { cmd_s     , get_s_arg     , free_s_arg    , 2 }, // find/replace/all that crazy s stuff
    ['t'] = { cmd_t     , get_bt_arg    , NULL          , 2 }, // if s/// succeeded (since input or last t) brance to label (end if no label)
    ['w'] = { cmd_w     , get_w_arg     , NULL          , 2 }, // append pattern space to file
    ['x'] = { cmd_x     , NULL          , NULL          , 2 }, // exchange pattern and hold spaces
    ['y'] = { cmd_y     , get_y_arg     , free_y_arg    , 2 }, // replace characters in set1 with characters in set2 (sets is two adjacent strings)
    [':'] = { cmd_colon , get_colon_arg , NULL          , 0 }, // defines label for later b and t commands
    ['='] = { cmd_equal , NULL          , NULL          , 1 }, // printf("%d\n", line_number);
    ['{'] = { cmd_lbrace, get_lbrace_arg, NULL          , 2 }, // if we match, run commands, otherwise jump to close
    ['}'] = { cmd_rbrace, get_rbrace_arg, NULL          , 0 }, // noop, hold onto open for ease of building scripts

    [CHAR_MAX    ] = { NULL    , NULL, NULL, 0 }, // fill out the rest of the array
    [CHAR_MAX + 1] = { cmd_last, NULL, NULL, 0 }
};

/*
 * Sedfuncs
 * Check if cmd is applicable, perform function if it is, return Action to run
 * after command
 */
Action cmd_a(Command *cmd)
{
    if (in_range(cmd) && !push(&writes, cmd))
        return error;
    return noop;
}

Action cmd_b(Command *cmd)
{
    if (!in_range(cmd))
        return noop;

    // if we jump backwards update to end, if we jump forwards update to there
    update_ranges(cmd + 1, cmd->jump > cmd ? cmd->jump : ni);
    pc = cmd->jump;
    return noop;
}

Action cmd_c(Command *cmd)
{
    if (!in_range(cmd))
        return noop;

    if (!cmd->in_match && check_puts(cmd->text, stdout))
        return error;
    return new_next;
}

Action cmd_d(Command *cmd)
{
    if (!in_range(cmd))
        return noop;

    *patt_space = '\0';
    return new_next;
}

Action cmd_D(Command *cmd)
{
    char *p;

    if (!in_range(cmd))
        return noop;

    if (!(p = strchr(patt_space, '\n')))
        return cmd_d(cmd);
    p++;
    memmove(patt_space, p, strlen(p) + 1);
    return old_next;
}

Action cmd_g(Command *cmd)
{
    if (in_range(cmd))
        strlcpy(patt_space, hold_space, SPACE_BYTES);
    return noop;
}

Action cmd_G(Command *cmd)
{
    if (!in_range(cmd))
        return noop;

    if (strlen(patt_space) + strlen(hold_space) + 1 > SPACE_BYTES)
        warn("truncating pattern space to %zu bytes", SPACE_BYTES);

    strlcat(patt_space, "\n"      , SPACE_BYTES);
    strlcat(patt_space, hold_space, SPACE_BYTES);
    return noop;
}

Action cmd_h(Command *cmd)
{
    if (in_range(cmd))
        strlcpy(hold_space, patt_space, SPACE_BYTES);
    return noop;
}

Action cmd_H(Command *cmd)
{
    if (!in_range(cmd))
        return noop;

    if (strlen(patt_space) + strlen(hold_space) + 1 > SPACE_BYTES)
        warn("truncating hold space to %zu bytes", SPACE_BYTES);

    strlcat(hold_space, "\n"      , SPACE_BYTES);
    strlcat(hold_space, patt_space, SPACE_BYTES);
    return noop;
}

Action cmd_i(Command *cmd)
{
    if (in_range(cmd) && check_puts(cmd->text, stdout))
        return error;
    return noop;
}

Action cmd_l(Command *cmd)
{
    if (!in_range(cmd))
        return noop;

    char *escapes[] = {
        ['\\'] = "\\\\", ['\a'] = "\\a", ['\b'] = "\\b",
        ['\f'] = "\\f" , ['\r'] = "\\r", ['\t'] = "\\t",
        ['\v'] = "\\v" , [UCHAR_MAX] = '\0', // fill out the table
    };

    for (char *p = patt_space; *p; p++) {
        // wrap lines to keep a POSIX compliant text file
        // (save room for '\\' and '\n')
        if (p != patt_space && (p - patt_space) % (LINE_MAX - 2) == 0)
            if (check_puts("\\", stdout))
                return error;
        if (escapes[(unsigned char)*p]) {
            if (printf("%s", escapes[(unsigned char)*p]) < 0) {
                serror();
                return error;
            }
        } else if (!isprint(*p)) {
            if (printf("\\%03hho", (unsigned char)*p) < 0) {
                serror();
                return error;
            }
        } else if (putchar(*p) == EOF) {
            serror();
            return error;
        }
    }
    if (check_puts("$", stdout))
        return error;
    return noop;
}

Action cmd_n(Command *cmd)
{
    if (!in_range(cmd))
        return noop;
    if (cmd_last(NULL) == error)
        return error;
    return new_line;
}

Action cmd_N(Command *cmd)
{
    if (!in_range(cmd))
        return noop;
    if (do_writes())
        return error;
    return app_line;
}

Action cmd_p(Command *cmd)
{
    if (in_range(cmd) && check_puts(patt_space, stdout))
        return error;
    return noop;
}

Action cmd_P(Command *cmd)
{
    char *p;
    if (!in_range(cmd))
        return noop;

    if ((p = strchr(patt_space, '\n')))
        *p = '\0';
    if (check_puts(patt_space, stdout))
        return error;
    if (p)
        *p = '\n';
    return noop;
}

Action cmd_q(Command *cmd)
{
    if (!in_range(cmd))
        return noop;
    if (cmd_last(NULL) == error)
        return error;
    return quit;
}

Action cmd_r(Command *cmd)
{
    if (in_range(cmd))
        if (!push(&writes, cmd))
            return error;
    return noop;
}

Action cmd_s(Command *cmd)
{
    if (!in_range(cmd))
        return noop;

    if (cmd->last_regex && !last_regex) {
        warn("no previous regex");
        return error;
    }

    regex_t   *re = cmd->last_regex ? last_regex : cmd->regex;
    regmatch_t pmatch[re->re_nsub + 1];
    char       buf[SPACE_BYTES], *new = buf, *str = patt_space;
    unsigned   matches = 0, last_empty = 1, qflag = 0;
    int        cflags  = 0;

    last_regex = re;

    while (!qflag && !regexec(re, str, LENGTH(pmatch), pmatch, cflags)) {
        cflags = REG_NOTBOL; // want to match against beg of line  first time, but not again
        if (!*str)           // want to match against empty string first time, but not again
            qflag = 1;
        if ((last_empty || pmatch[0].rm_eo) &&  // don't subst if last match was not empty and this one is
            (++matches == cmd->occurrence || !cmd->occurrence)) {  // correct match or global
            // copy over everything before the match
            if (sized_memcpy(&new, str, pmatch[0].rm_so, buf, sizeof(buf)))
                break;

            size_t len;
            for (char *p = cmd->text; (len = strcspn(p, "\\&")) || *p; ++p) {
                if (sized_memcpy(&new, p, len, buf, sizeof(buf)))
                    break;
                p += len;
                switch(*p) {
                default  : --p; break;
                case '\\': if (isdigit(*++p)) {
                               if ((size_t)(*p - '0') > re->re_nsub)
                                   warn("back reference number (%c) greater than number of groups (%zu)",
                                           *p, re->re_nsub);

                               regmatch_t *rm = &pmatch[*p - '0'];
                               if (rm->rm_so != -1)
                                   sized_memcpy(&new, str + rm->rm_so, rm->rm_eo - rm->rm_so, buf, sizeof(buf));
                           } else
                               sized_memcpy(&new, p, 1, buf, sizeof(buf));
                           break;
                case '&' : sized_memcpy(&new, str + pmatch->rm_so, pmatch->rm_eo - pmatch->rm_so, buf, sizeof(buf));
                           break;
                }
            }
        } else {
            // copy over everything including the match
            if (sized_memcpy(&new, str, pmatch[0].rm_eo, buf, sizeof(buf)))
                break;
        }
        if (!pmatch[0].rm_eo)    // empty match
            *(new++) = *(str++); // advance one character, add it to output
        last_empty = !pmatch[0].rm_eo;
        str       +=  pmatch[0].rm_eo;
    }
    gflags.s |= matches && matches >= cmd->occurrence;
    if (!(matches && matches >= cmd->occurrence))
        return noop;

    sized_memcpy(&new, str, strlen(str) + 1, buf, sizeof(buf));
    strlcpy(patt_space, buf, SPACE_BYTES);
    if (cmd->flag_p && check_puts(patt_space, stdout))
        return error;
    if (cmd->file && check_puts(patt_space, cmd->file))
        return error;
    return noop;
}

Action cmd_t(Command *cmd)
{
    if (!in_range(cmd) || !gflags.s)
        return noop;

    // if we jump backwards update to end, if we jump forwards update to there
    update_ranges(cmd + 1, cmd->jump > cmd ? cmd->jump : ni);
    pc = cmd->jump;
    gflags.s = 0;
    return noop;
}

Action cmd_w(Command *cmd)
{
    if (in_range(cmd) && check_puts(patt_space, cmd->file))
        return error;
    return noop;
}

Action cmd_x(Command *cmd)
{
    if (!in_range(cmd))
        return noop;

    char *tmp  = patt_space;
    patt_space = hold_space;
    hold_space = tmp;
    return noop;
}

Action cmd_y(Command *cmd)
{
    if (!in_range(cmd))
        return noop;

    char *find = cmd->text;
    char *repl = find + strlen(find) + 1;

    for (char *p = patt_space; (p = strpbrk(p, find)); p++)
        *p = repl[strchr(find, *p) - find];

    return noop;
}

Action cmd_colon(Command *cmd)
{
    USE(cmd);
    return noop;
}

Action cmd_equal(Command *cmd)
{
    if (!in_range(cmd))
        return noop;
    if (printf("%zu\n", line_number) < 0) {
        serror();
        return error;
    }
    return noop;
}

Action cmd_lbrace(Command *cmd)
{
    if (in_range(cmd))
        return noop;

    // update ranges on all commands we're skipping
    Command *jump = prog + cmd->offset;
    update_ranges(cmd + 1, jump);
    pc = jump;
    return noop;
}

Action cmd_rbrace(Command *cmd)
{
    USE(cmd);
    return noop;
}

// not actually a Sedfunc/cmd_ but acts like one
// command at end of script, print if not -n, perform scheduled a,r writes,
// start new cycle
Action cmd_last(Command *cmd)
{
    USE(cmd);
    if (!gflags.n && check_puts(patt_space, stdout))
        return error;
    if (do_writes())
        return error;
    return new_next;
}

/*
 * Getargs / Freeargs
 * Read the first argument from str into cmd->extra and return pointer to one
 * past last character of argument or NULL on error
 */
// prepare for text for a,c,i functions, set aci_cont flag so build() will read
// text instead of parsing a command
char *get_aci_arg(Command *cmd, char *str)
{
    cmd->print = check_puts;
    if (!(cmd->text = calloc((cmd->size = 1), 1))) { // start with empty string (1 nul byte)
        serror();
        return NULL;
    }
    if (*str != '\\' || *(str + 1) != '\0') {
        warn("trailing characters: %s", str);
        return NULL;
    }
    gflags.aci_cont = 1;
    return str + 1;
}

void free_acir_arg(Command *cmd)
{
    free(cmd->text);
}

// read label for b,t commands. for strict POSIX compliance read rest of line
// (including spaces and semicolons) and truncate to 8 bytes
// FIXME: POSIX mandates space after b,t
char *get_bt_arg(Command *cmd, char *str)
{
    size_t len = strlen(str = chomp(str));

    if (*str)
        strlcpy(cmd->label, str, LABEL_BYTES);

    if (*str && len > LABEL_BYTES - 1)
        warn("truncating label '%s' to '%s' (%zu bytes)", str, cmd->label, LABEL_BYTES);
    if (!push(&branches, (Command *)(cmd - prog)))
        return NULL;
    return str + len;
}

// read label for : command. for strict POSIX compliance read rest of line
// (including spaces and semicolons) and truncate to 8 bytes
char *get_colon_arg(Command *cmd, char *str)
{
    size_t len = strlen(str = chomp(str));

    if (!*str) {
        warn("no label name");
        return NULL;
    }
    strlcpy(cmd->label, str, LABEL_BYTES);
    if (len > LABEL_BYTES - 1)
        warn("truncating label '%s' to '%s' (%zu bytes)", str, cmd->label, LABEL_BYTES);
    if (!push(&labels, (Command *)(cmd - prog)))
        return NULL;
    return str + len;
}

// push address of { so we can pop for }. not really getarg, but called at the
// same place. (use cmd - prog offset instead of address so we don't screw up
// pointers when we realloc)
char *get_lbrace_arg(Command *cmd, char *str)
{
    if (!push(&braces, (Command *)(cmd - prog)))
        return NULL;
    return str;
}

// pop { and set offset to cmd so we can jump from { to }
char *get_rbrace_arg(Command *cmd, char *str)
{
    Command *lbrace = prog + (ptrdiff_t)pop(&braces);

    if (!lbrace) {
        warn("extra }");
        return NULL;
    }
    str = chomp(str);
    if (*str) {
        warn("trailing characters, } must be on its own line");
        return NULL;
    }
    lbrace->offset = cmd - prog;
    return str;
}

// read file for r command. for strict POSIX compliance read rest of line
// (including spaces and semicolons)
char *get_r_arg(Command *cmd, char *str)
{
    if (!isblank(*str)) {
        warn("no space before file name");
        return NULL;
    }
    cmd->print = write_file;
    str        = chomp(str);
    if (!*str) {
        warn("no file name");
        return NULL;
    }
    if (!(cmd->text = strdup(str))) {
        serror();
        return NULL;
    }
    return str + strlen(str);
}

// read arguments for s function, replace "\\n" with literal newline "\n" in
// regex. if w flag read rest of line as file name for strict POSIX compliance
// (including spaces and semicolons)
char *get_s_arg(Command *cmd, char *str)
{
    char  *p, *q, *beg = str, delim = *str, *repl_escaped = "\\123456789&";
    int    err;
    size_t len;

    // s_Find_Replace_Flags

    // Find
    // we are NOT continuing from a literal newline in a replacement text
    if (!gflags.s_cont) {
        cmd->occurrence = 1;
        if (!delim || delim == '\\') {
            warn("bad delimiter '%c' in s argument: %s", delim, beg);
            return NULL;
        }
        if (*++str == delim) // empty regex, use last_regex
            cmd->last_regex = 1;
        if (!(p = find_delim(str, bre_escaped, delim, 1)))
            return NULL;
        if (!*p) {
            warn("missing second delimiter '%c' in s argument: %s", delim, beg);
            return NULL;
        }
        p -= handle_escapes(str, p, delim, 0);
        *p = '\0';
        if (!cmd->last_regex) {
            if (!(cmd->regex = malloc(sizeof(*cmd->regex)))) {
                serror();
                return NULL;
            }
            if ((err = regcomp(cmd->regex, str, 0))) {
                char msg[128]; // TODO: size? own regcomp() function so no repeats
                regerror(err, cmd->regex, msg, sizeof(msg));
                warn("bad regex: %s: %s", str, msg);
                return NULL;
            }
        }
        *(p++) = cmd->delim = delim; // replace delim for error messages
    } else { // continuing after a literal newline in the replacment text
        p     = str;
        delim = cmd->delim;
    }

    // Replace
    // if we are continuing from a literal newline in a replacement text p
    // points to the beginning of the current line
    // if we are not continuing p points one character past to the second
    // delimiter
    if (!(q = find_delim(p, repl_escaped, delim, 0)))
        return NULL;
    if (!*q) { // no third delimiter, either backslash newline or error
        len = q-- - p;
        if (*q != '\\') {
            warn("missing third delimiter '%c' in s argument: %s", delim, beg);
            return NULL;
        }
        gflags.s_cont = 1;
        *(q++) = '\n';
        --ni; // decrement next inst pointer so when it's incremented it points to cmd again
    } else {
        gflags.s_cont = 0;
        len = q - p;
    }
    if (!cmd->size)
        ++len; // need space for nul byte first time
    *q = '\0';

    q = p; // check for bad back references
    for (int escape = 0; *q; q++) {
        if (escape) {
            escape = 0;
            if (isdigit(*q) && !cmd->last_regex && (size_t)(*q - '0') > cmd->regex->re_nsub) {
                warn("back reference number (%c) greater than number of groups (%zu)",
                     *q, cmd->regex->re_nsub);
                return NULL;
            }
        }
        else if (*q == '\\')
            escape = 1;
    }
    if (resize((void **)&cmd->text, &cmd->size, sizeof(*cmd->text), cmd->size + len, NULL, 1)) {
        serror();
        return NULL;
    }
    strlcat(cmd->text, p, cmd->size);
    if (gflags.s_cont) // literal newline in replacement text
        return q;
    else
        *q = delim; // replace delim for better error messages

    // Flags
    // q now points to the third delimiter
    unsigned occ_flag = 0;
    for (p = q + 1; *p; p++) {
        if (isdigit(*p)) {
            if (occ_flag++) {
                warn("specifying global and an occurence is undefined");
                return NULL;
            }
            long num = strtol(p, &p, 10);
            if (num == LONG_MAX)
                serror();
            if (num == 0) {
                warn("invalid match number: 0");
                return NULL;
            }
            cmd->occurrence = num;
            p--;
        } else switch (*p) {
            default : return p;
            case 'p': cmd->flag_p     = 1; break;
            case 'g': cmd->occurrence = 0;
                      if (occ_flag++) {
                          warn("specifying global and an occurence is undefined");
                          return NULL;
                      }
                      break;
            case 'w':
            {
                Command buf;
                if (!(p = get_w_arg(&buf, p + 1)))
                    return NULL;
                cmd->file = buf.file;
                break;
            }
        }
        if (!*p) // w flag ate rest of line
            break;
    }

    // p now points to 1 past last argument
    return p;
}

void free_s_arg(Command *cmd)
{
    free(cmd->text);
    if (!cmd->last_regex && cmd->regex) {
        regfree(cmd->regex);
        free(cmd->regex);
    }
}

// read file name for w function for strict POSIX compliance read rest of line
// (including spaces and semicolons) and limit to MAX_WFILES wfiles
char *get_w_arg(Command *cmd, char *str)
{
    unsigned i;

    if (!isblank(*str)) {
        warn("no space before file name");
        return NULL;
    }
    str = chomp(str);
    if (!*str) {
        warn("no file name");
        return NULL;
    }
    for (i = 0; i < MAX_WFILES && wfiles[i].file; i++)
        if (!strcmp(str, wfiles[i].name))
            break;
    if (i == MAX_WFILES) {
        warn("too many wfiles");
        return NULL;
    }
    if (!wfiles[i].file) { // no match
        wfiles[i].name = strdup(str);
        if (!(wfiles[i].file = fopen(str, "w"))) {
            serror();
            return NULL;
        }
    }
    cmd->file = wfiles[i].file;
    return str + strlen(str);
}

// read find and replace sets for y function, put in cmd->text as two adjacent
// nul terminated strings (i.e. char *set2 = cmd->text + strlen(cmd->text) + 1)
char *get_y_arg(Command *cmd, char *str)
{
    char *p, *q, *beg = str, delim = *str, *allow = "n\\";

    if (delim == '\\') {
        warn("bad delimiter '%c' for y command", delim);
        return NULL;
    }
    if (!(p = find_delim(str + 1, allow, delim, 0)))
        return NULL;
    if (!*p) {
        warn("missing second delimiter '%c' for y command: %s", delim, beg);
        return NULL;
    }
    if (!(q = find_delim(p + 1, allow, delim, 0)))
        return NULL;
    if (!*q) {
        warn("missing second delimiter '%c' for y command: %s", delim, beg);
        return NULL;
    }
    q -= handle_escapes(str, q, delim, 1);
    p  = find_delim(str + 1, allow, delim, 0);
    if (q - p != p - str) {
        warn("bad y argument, different length sets: %s", beg);
        return NULL;
    }
    *q = '\0';
    if (!(cmd->text = strdup(str + 1))) {
        serror();
        return NULL;
    }
    *find_delim(cmd->text, allow, delim, 0) = '\0';
    return q + 1;
}

void free_y_arg(Command *cmd)
{
    free(cmd->text);
}

/*
 * Actions
 * Read, append, print, reset pc, etc.
 * Return -1 on error, 0 to continue execution, 1 to halt
 */
// append new line, continue current cycle
int app_line(void)
{
    char line[LINE_MAX + 1];

    while (read_line(line, sizeof(line), file) == EOF)
        if (next_file())
            return 1;

    if (strlen(patt_space) + strlen(line) + 1 > SPACE_BYTES)
        warn("truncating pattern space to %zu bytes", SPACE_BYTES - 1);

    strlcat(patt_space, "\n", SPACE_BYTES);
    strlcat(patt_space, line, SPACE_BYTES);
    gflags.s = 0;
    line_number++;
    return 0;
}

// he's dead Jim
int error(void)
{
    return -1;
}

// read new line, continue current cycle
int new_line(void)
{
    while (read_line(patt_space, LINE_MAX + 1, file) == EOF)
        if (next_file())
            return 1;
    gflags.s = 0;
    line_number++;
    return 0;
}

// move to new cycle, read new line
int new_next(void)
{
    int ret;

    // update ranges on all commands we're skipping
    update_ranges(pc + 1, ni);
    ret = new_line();
    pc  = prog - 1;
    return ret;
}

// continue execution
int noop(void)
{
    return 0;
}

// move to new cycle, reuse pattern space
int old_next(void)
{
    // update ranges on all commands we're skipping
    update_ranges(pc + 1, ni);
    pc = prog - 1;
    return 0;
}

// do not start a new cycle
int quit(void)
{
    return 1;
}

/*
 * Utilities
 */
// write str to stream followed by newline and check for errors
int check_puts(char *str, FILE *stream)
{
    if (fputs(str, stream) == EOF) {
        serror();
        return -1;
    }
    if (fputs("\n", stream) == EOF) {
        serror();
        return -1;
    }
    return 0;
}

// return pointer to first non blank character in str
char *chomp(char *str)
{
    for (; *str && isblank(*str); str++)
        ;
    return str;
}

// given memory pointed to by *ptr that currently holds *nmemb members of size
// size, realloc to hold new_nmemb members, return new_nmemb in *memb and one
// past old end in *next. if clear is nonzero clear new memory. if realloc fails
// change nothing. (should work to shrink, too...)
int resize(void **ptr, size_t *nmemb, size_t size, size_t new_nmemb, void **next, int clear)
{
    void *n, *tmp;

    if (new_nmemb) {
        if (!(tmp = realloc(*ptr, new_nmemb * size)))
            return -1;
    } else { // turns out realloc(*ptr, 0) != free(*ptr)
        free(*ptr);
        tmp = NULL;
    }
    n = (char *)tmp + *nmemb * size;
    if (clear && new_nmemb > *nmemb)
        memset(n, 0, (new_nmemb - *nmemb) * size);
    *nmemb = new_nmemb;
    *ptr   = tmp;
    if (next)
        *next = n;
    return 0;
}

// Find first delim in str that is not escaped or in []
// or in [::] inside [] or in [..] inside []
// return NULL on error (undefined escaped sequence)
// return pointer to trailing nul byte if no delim found
char *find_delim(char *str, char *allowed, char delim, int do_brackets)
{
    enum {
        OUTSIDE         , // not in brackets
        BRACKETS_OPENING, // last char was first [ or last two were first [^
        BRACKETS_INSIDE , // inside []
        INSIDE_OPENING  , // inside [] and last char was [
        CLASS_INSIDE    , // inside class [::], or colating element [..] or [==], inside []
        CLASS_CLOSING   , // inside class [::], or colating element [..] or [==], and last character was the respective : . or =
    }        brackets = OUTSIDE;
    unsigned escape = 0;
    char     *p, c = 0; // : inside class, . or = inside colating element

    // TODO: is there a better way? this is a big unwieldy state machine
    for (p = str; *p; p++) {
        if      (brackets == BRACKETS_OPENING       &&  *p == '^'  ) {                             continue; }
        else if (brackets == BRACKETS_OPENING       &&  *p == ']'  ) { brackets = BRACKETS_INSIDE; continue; }
        else if (brackets == BRACKETS_OPENING                      ) { brackets = BRACKETS_INSIDE;           }

        if      (brackets == CLASS_CLOSING          &&  *p == ']'  ) { brackets = BRACKETS_INSIDE ;          }
        else if (brackets == CLASS_CLOSING                         ) { brackets = CLASS_INSIDE    ;          }
        else if (brackets == CLASS_INSIDE           &&  *p ==  c   ) { brackets = CLASS_CLOSING   ;          }
        else if (brackets == INSIDE_OPENING         && (*p == ':'  ||
                                                        *p == '.'  ||
                                                        *p == '=') ) { brackets = CLASS_INSIDE    ; c = *p;  }
        else if (brackets == INSIDE_OPENING         &&  *p == ']'  ) { brackets = OUTSIDE         ;          }
        else if (brackets == BRACKETS_INSIDE        &&  *p == '['  ) { brackets = INSIDE_OPENING  ;          }
        else if (brackets == BRACKETS_INSIDE        &&  *p == ']'  ) { brackets = OUTSIDE         ;          }
        else if (brackets == OUTSIDE                &&  escape     ) { if (strchr(allowed, *p) || *p == delim)
                                                                           escape = 0;
                                                                       else {
                                                                           warn("undefined result escaped '%c': %s", *p, str);
                                                                           return NULL;
                                                                       }                                     }
        else if (brackets == OUTSIDE                &&  *p == '\\' ) { escape   = 1               ;          }
        else if (brackets == OUTSIDE && do_brackets &&  *p == '['  ) { brackets = BRACKETS_OPENING;          }
        else if (brackets == OUTSIDE                &&  *p == delim) return p;
    }
    return p;
}

// scan from beg to end, replacing "\\d" with "d" and "\\n" with "\n" (where d
// is delim) then memmove rest of string into place
// special behavior if d is '\0' then remove all backslashes
// if d is "n" and n_newline is 0 then "\\n" is replaced with "n"
// if d is "n" and n_newline is 1 then "\\n" is replaced with "\n"
// (s and y handle it differently...)
size_t handle_escapes(char *beg, char *end, char d, int n_newline)
{
    size_t num_escapes = 0;
    char *src = beg, *dst = beg;

    while (src < end && (*(dst++) = *(src++))) {
        if (*(src - 1) == '\\' && *src == '\\') {
            // take care of a double backslash, otherwise we'll think the second
            // backslash is escaping something
            if (src < end && !(*(dst++) = *(src++)))
                break;
        } else if (*(src - 1) == '\\' && (*src == 'n' || *src == d || d == '\0')) {
            if (*src == 'n')
                *(dst - 1) = (d == 'n' && !n_newline) ? 'n' : '\n';
            else if (*src == d || d == '\0')
                *(dst - 1) = *src;
            ++src;
            ++num_escapes;
        }
    }
    memmove(dst, src, strlen(src) + 1);
    return num_escapes;
}

// test of stream is at EOF
int is_eof(FILE *stream)
{
    if (!stream || feof(stream))
        return 1;

    int c = fgetc(stream);
    if (c == EOF && ferror(stream))
        serror();
    if (c != EOF && ungetc(c, stream) == EOF)
        serror();

    return c == EOF;
}

// move to next file on command line or stdin if this is the first call and
// there are no files on the command line
// return 0 for success, 1 for no more files
int next_file(void)
{
    static unsigned first = 1;

    if (file == stdin)
        clearerr(file);
    else if (file && fclose(file))
        warn("Failed to properly close '%s': %s", *(files - 1), strerror(errno));
    file = NULL;

    do {
        if (!*files) {
            if (first) // given no files, use stdin
                file = stdin;
        } else if (!strcmp(*files, "-")) {
            file = stdin;
            files++;
        } else if (!(file = fopen(*(files++), "r")))
            warn("Failed to open '%s': %s", *(files - 1), strerror(errno));
    } while (!file && *files);
    first = 0;
    return !file;
}

// pop cmd from vec, shrinking if we're using < 1/4 of capcity
Command *pop(Vector *vec)
{
    Command *tmp;

    if (!vec->size)
        return NULL;
    tmp = vec->data[--vec->size];

    if (vec->size <= vec->capacity / 4)
        if (resize((void **)&vec->data, &vec->capacity, sizeof(*vec->data), vec->capacity / 2, NULL, 0))
            return NULL;
    return tmp;
}

// push cmd onto vec, growing (doubling) if needed
size_t push(Vector *vec, Command *cmd)
{
    if (vec->size == vec->capacity)
        if (resize((void **)&vec->data, &vec->capacity, sizeof(*vec->data), vec->capacity * 2 + 1, NULL, 0))
            return 0;
    vec->data[vec->size++] = cmd;
    return vec->size;
}

// read one line (max size bytes including '\0') from file into buf. warn if
// line is truncated
int read_line(char *buf, size_t size, FILE* file)
{
    size_t len;
    if (!file)
        return EOF;
    if (fgets(buf, size, file) == NULL) {
        if (ferror(file))
            serror();
        return EOF;
    }
    len = strlen(buf) - 1;
    if (buf[len] == '\n') {
        buf[len] =  '\0';
    } else if (feof(file)) {
        warn("no final newline, not POSIX text file");
    } else {
        warn("truncating line to %zu bytes", size - 1);
        do {
            if (fgets(buf, size, file) == NULL) {
                if (ferror(file))
                    serror();
                return EOF;
            }
        } while (buf[strlen(buf) - 1] != '\n');
    }
    return 0;
}

// memcpy from src to dest min(n, buf_size - (dest - buf)), warn if truncating
// dest is a pointer to a location inside buf (i.e. dest - buf <= buf_size)
int sized_memcpy(char **dest, char *src, size_t n, char *buf, size_t buf_size)
{
    // set to pc and only warn about truncating once per command
    static Command *cmd = NULL;
    int ret = 0;

    if (*dest - buf + n > buf_size) {
        if (cmd != pc)
            warn("truncating pattern space to %zu bytes", buf_size);
        n   = buf_size - (*dest - buf);
        ret = -1;
    }
    memcpy(*dest, src, n);
    *dest += n;
    cmd    = pc;
    return ret;
}

void strlcpy(char *dest, char *src, size_t size)
{
    if (!size || !dest || !src)
        return;

    while (--size && (*(dest++) = (*src++)))
        ;
    if (!size)
        *dest = '\0';
}

void strlcat(char *dest, char *src, size_t size)
{
    size_t len;

    if (!size || !dest || !src)
        return;
    if ((len = strlen(dest)) >= size)
        return;
    dest += len;
    size -= len;
    strlcpy(dest, src, size);
}

// read file at in_path a line at a time (truncating if long lines) and write
// to out_stream
int write_file(char *in_path, FILE *out_stream)
{
    char line[LINE_MAX + 1];
    FILE *in_stream = fopen(in_path, "r");

    if (!in_stream) // no file is treated as empty file
        return 0;
    while (read_line(line, sizeof(line), in_stream) != EOF)
        if (check_puts(line, out_stream))
            return -1;
    if (fclose(in_stream))
        serror(); // TODO: return error? or just keep going?
    return 0;
}

/*
 * Build and run
 */
// append line to the text for the given a,c,i command
int aci_append(Command *cmd, char *line)
{
    char  **text = &cmd->text;
    size_t *size = &cmd->size;
    size_t  len  = strlen(line);

    if (!len) {// empty line
        gflags.aci_cont = 0;
        return 0;
    }
    if (line[len - 1] == '\\')
        line[len - 1] =  '\n';
    else
        gflags.aci_cont = 0;

    // get rid of all backslashes
    len -= handle_escapes(line, line + len, 0, 0);

    // already have 1 null byte
    if (resize((void **)text, size, sizeof(**text), *size + len, NULL, 0)) {
        serror();
        return -1;
    }
    strlcat(*text, line, *size);
    return 0;
}

// read given line building the program. return 0 on success -1 on error
// TODO: table driven state machine?
int build(char *line)
{
    char orig[strlen(line) + 1]; // for better error messages
    strlcpy(orig, line, sizeof(orig));

    // if the script starts with #n it shall be equivalent to specifying -n
    if (++line_number == 1 && line[0] == '#' && line[1] == 'n')
        gflags.n = 1;
    if (gflags.aci_cont) {
        if (aci_append(ni - 1, line))
            return -1;
    } else for (char *p = line; *p;) {
        if (!gflags.s_cont) { // if we have an s replacement continuation, skip to the getarg()
            if (ni == prog + prog_size)
                if (resize((void **)&prog, &prog_size, sizeof(*prog), prog_size * 2 + 1, (void **)&ni, 1))
                    return -1;

            for (; isblank(*p) || *p == ';'; p++)
                // inside {} only one command per line optionally preceded by blanks
                if (braces.size && *p == ';')
                    warn("leading semicolon inside {}: %s", p);
            if (!*p || *p == '#')
                break;
            if (!(p = make_range(&ni->range, p)))
                return -1;
            p = chomp(p);
            if (*p == '!')
                ni->negate = 1;
            for (; *p == '!'; p++)
                ;
            if (!(ni->func = &funcs[(int)*p])->runcmd) { // stupid -Wchar-subcripts
                warn("%s", orig);
                warn("%*s^ bad sed function: %c", (int)(p - line), "", *p);
                return -1;
            }
            if (ni->range.naddr > ni->func->naddr) {
                warn("%s", orig);
                warn("%*s^ function %c only takes %d addr", (int)(p - line), "", *p, ni->func->naddr);
                return -1;
            }
            p++;
        }
        if (ni->func->getarg && !(p = ni->func->getarg(ni, p)))
            return -1;

        if (braces.size && (ptrdiff_t)braces.data[braces.size - 1] == ni - prog) {
            // we just opened a brace, we can have blanks and then another command,
            // skip the following checks
        } else if (braces.size && *p) {
            // inside {} only one command per line immediately followed by a newline
            // except for the same line as the {
            warn("%s", orig);
            warn("%*s^ trailing characters (%c) while inside {}", (int)(p - line), "", *p);
            return -1;
        } else if (*p && *p != ';') {
            // commands may be followed immediately by a semicolon, then blanks
            // commands that cannot be followed by a semicolon will cause *p == '\0'
            warn("%s", orig);
            warn("%*s^ trailing non semicolon characters (%c)", (int)(p - line), "", *p);
            return -1;
        }
        ni++;
    }
    return 0;
}

void cleanup(void)
{
    free(braces  .data);
    free(labels  .data);
    free(branches.data);

    for (unsigned i = 0; i < MAX_WFILES && wfiles[i].file; i++) {
        free(wfiles[i].name);
        if (fclose(wfiles[i].file))
            serror();
    }

    for (pc = prog; pc < ni; pc++) {
        if (pc->range.beg.type == REGEX && pc->range.beg.regex) {
            regfree(pc->range.beg.regex);
            free   (pc->range.beg.regex);
        }
        if (pc->range.end.type == REGEX && pc->range.end.regex) {
            regfree(pc->range.end.regex);
            free   (pc->range.end.regex);
        }
        if (pc->func->freearg)
            pc->func->freearg(pc);
    }

    free(prog);
}

// perform writes scheduled by a and r commands
int do_writes(void)
{
    Command *cmd;

    for (unsigned i = 0; i < writes.size; i++) {
        cmd = writes.data[i];
        if(cmd->print(cmd->text, stdout))
            return -1;
    }
    // sure I could while(pop()); but... eh
    if (resize((void **)&writes.data, &writes.capacity, sizeof(*writes.data), 0, NULL, 0))
        return -1;
    writes.size = 0;
    return 0;
}

Command *find_label(Vector *vec, char *label)
{
    for (unsigned i = 0; i < vec->size; i++)
        if (!strcmp(label, (prog + (ptrdiff_t)vec->data[i])->label))
            return prog + (ptrdiff_t)vec->data[i];
    return NULL;
}

// test if current line is within cmd->range taking into account cmd->negate
int in_range(Command *cmd)
{
    if (match_addr(&cmd->range.beg)) {
        if (cmd->range.naddr == 2) {
            if (cmd->range.end.type == LINE && cmd->range.end.line_number <= line_number)
                cmd->in_match = 0;
            else
                cmd->in_match = 1;
        }
        return !cmd->negate;
    }
    if (cmd->in_match && match_addr(&cmd->range.end)) {
        cmd->in_match = 0;
        return !cmd->negate;
    }
    return cmd->in_match ^ cmd->negate;
}

// make jump for b,t point to corresponding label
int insert_labels(void)
{
    Command *cmd, *label;

    // side effect of stuffing ptrdiff_t into Command*, NULL (0) is a valid
    // value, so check size explicitly
    while (branches.size) {
        cmd = prog + (ptrdiff_t)pop(&branches);
        if (!*cmd->label) // no label, branch to end of script
            label = ni - 2;
        else
            label = find_label(&labels, cmd->label);

        if (!label) {
            warn("no such label: %s", cmd->label);
            return -1;
        }
        cmd->jump = label;
    }
    return 0;
}

// Read the first address from str into addr and return pointer to character 1
// past end of address or NULL on error
char *make_address(Address *addr, char *str)
{
    char *end = NULL, *orig = str;

    if (*str == '$') {
        addr->type = LAST;
        end = str + 1;
    } else if (isdigit(*str)) { // line number
        // TODO: strtol func so I don't repeat it
        long num = strtol(str, &end, 10);
        if (num == LONG_MAX)
            serror();
        if (num == 0) {
            warn("unsupported address: 0");
            return NULL;
        }
        *addr = (Address){ .type = LINE, .line_number = num };
    } else if (*str == '/' || *str == '\\') {
        char delim;
        if (*str == '\\' && *++str == '\\') {
            warn("bad delimiter: %s", orig);
            return NULL;
        }
        delim = *str;
        if (*++str == delim) {
            addr->type = LASTRE;
            end = str + 1;
        } else if (!(end = find_delim(str, bre_escaped, delim, 1))) {
            return NULL;
        } else if (!*end) {
            warn("unclosed regex: %s", orig);
            return NULL;
        } else {
            int err;
            end -= handle_escapes(str, end, delim, 0);
            *(end++) = '\0';
            addr->type = REGEX;
            if (!(addr->regex = malloc(sizeof(*addr->regex)))) {
                serror();
                return NULL;
            }
            if ((err = regcomp(addr->regex, str, 0))) {
                char msg[128]; // TODO: size?
                regerror(err, addr->regex, msg, sizeof(msg));
                warn("bad regex: %s: %s", str, msg);
                end = NULL;
            }
        }
    } else {
        addr->type = EVERY;
        end = str;
    }
    return end;
}

// Read the first range from str in range and return pointer to character 1
// past end of range or NULL on error
char *make_range(Range *range, char *str)
{
    char *p = str;

    if (!(p = make_address(&range->beg, p)))
        return NULL;

    if (*p != ',')
        range->end.type = IGNORE;
    else if (!(p = make_address(&range->end, p + 1)))
        return NULL;

    if      (range->beg.type == EVERY  && range->end.type == IGNORE)
        range->naddr = 0;
    else if (range->beg.type != IGNORE && range->end.type == IGNORE)
        range->naddr = 1;
    else if (range->beg.type != IGNORE && range->end.type != IGNORE)
        range->naddr = 2;
    else { // This should never happen right?
        warn("bad range: %s", str);
        return NULL;
    }

    return p;
}

// test if addr matches current pattern space
int match_addr(Address *addr)
{
    switch(addr->type) {
    default    :
    case IGNORE: return 0;                                             // empty address, ignore
    case EVERY : return 1;                                             // every line
    case LINE  : return line_number == addr->line_number;              // line number
    case LAST  : while (is_eof(file) && !next_file())                  // last line ($)
                     ;
                 return !file;
    case REGEX : last_regex = addr->regex;
                 return !regexec(addr->regex, patt_space, 0, NULL, 0); // use included regex
    case LASTRE: if (!last_regex) {
                     warn("no previous regex");
                     exit(EXIT_FAILURE); // TODO: is there a better way?
                 }
                 return !regexec(last_regex , patt_space, 0, NULL, 0); // use most recently used regex
    }
}
// finishing touches, initial setup, run the program
// quit when a command returns a function that returns non zero
int run(void)
{
    int ret;

    if (gflags.aci_cont) {
        warn("text for a, c, or i command not finished");
        return -1;
    }

    line_number = 0;
    if (braces.size) {
        warn("extra {");
        return -1;
    }
    if (insert_labels()) return -1;
    if (next_file    ()) return -1;
    if (new_line     ()) return  1; // no lines

    for (pc = prog; !(ret = pc->func->runcmd(pc)()); pc++)
        ;

    return ret;
}

// read lines from file at path and build() each line
int script_file(char *path)
{
    char  line[LINE_MAX + 1];
    FILE *file = fopen(path, "r");
    int   err  = 0;

    if (!file) {
        serror();
        return -1;
    }
    while (read_line(line, sizeof(line), file) != EOF)
        if ((err = build(line)))
            break;
    if (fclose(file))
        serror();
    return err;
}

// read lines from script and build() each line
int script_string(char *script)
{
    char line[LINE_MAX + 1];
    int  err = 0;

    // FIXME: 0 length script is still an empty line
    // FIXME: copy becase build() does funky stuff and strtok's saveptr may be wrong
    for (char *p = strtok(script, "\n"); p; p = strtok(NULL, "\n")) {
        if (strlen(p) > sizeof(line))
            warn("truncating line to %zu bytes", sizeof(line));
        strlcpy(line, p, sizeof(line));
        if ((err = build(line)))
            break;
    }
    return err;
}

// iterate from Command beg to end updating ranges so we don't miss any
// e.g. sed -n '1d;1,3p' should still print lines 2 and 3
void update_ranges(Command *beg, Command *end)
{
    while (beg < end)
        in_range(beg++);
}

int main(int argc, char **argv)
{
    int c, script = 0;

    prog_name = argv[0];

    if (argc == 1) {
        warn("USAGE:... at some point");
        return EXIT_FAILURE;
    }

    if (atexit(cleanup)) {
        serror();
        return EXIT_FAILURE;
    }

    // -e script, -f file, -n
    while ((c = getopt(argc, argv, ":e:f:n")) != -1) {
        switch (c) {
            case 'n': gflags.n = 1;                                                break;
            case 'e': if (script_string(optarg)) return EXIT_FAILURE; script++;    break;
            case 'f': if (script_file  (optarg)) return EXIT_FAILURE; script++;    break;
            case ':': fprintf(stderr, "Option -%c requires an operand\n", optopt); return EXIT_FAILURE;
            case '?': fprintf(stderr, "Unrecognized option: -%c\n"      , optopt); return EXIT_FAILURE;
        }
    }

    if (!script && optind == argc) {
        warn("USAGE:... at some point");
        return EXIT_FAILURE;
    } else if (!script && script_string(argv[optind++]))
        return EXIT_FAILURE;

    // add our last instruction
    if (ni == prog + prog_size)
        if (resize((void **)&prog, &prog_size, sizeof(*prog), prog_size + 1, (void **)&ni, 1))
            return EXIT_FAILURE;

    ni->range.beg.type = EVERY;
    ni->range.end.type = IGNORE;
    ni->func           = &funcs[LENGTH(funcs) - 1];
    ++ni;

    files = &argv[optind];

    return run() < 0 ? EXIT_FAILURE : EXIT_SUCCESS;
}
