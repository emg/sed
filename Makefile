.POSIX:

.c:
	gcc $(CFLAGS) $(LDFLAGS) -std=c11 -pedantic -Wall -Wextra -Os -o $@ $<

sed: Makefile

test: sed
	cd tests && $(MAKE)

clean:
	rm -f sed
	cd tests && $(MAKE) clean
